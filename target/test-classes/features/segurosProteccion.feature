@tag
Feature: Title of your feature
  I want to use this template for my feature file

  @proteccion
  Scenario: Seguros Proteccion
    Given Quiero acceder a la pagina de condiciones de uso
    When Diligencio los campos nombres y apellidos
      | nombres| apellido1 | apellido2| 
      | Daniela| Rojas     | Rojas    |
    And Accedo al link mas ventajas
    And Diligencio tipo y numero de documento
      |tipo|documento |
      |CC  |1020444448|
    And Diligencio la fecha
      |fecha     |
      |28/03/2019|
    Then Valido la salida