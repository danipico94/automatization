@tag
Feature: HospitalAdimin
  I want to use this template for my feature file

  @SuccessAddDoctor
  Scenario: Add new doctor
    Given I want to access the doctor form
    When A new doctor is hired
      | name    | lastname | telephone  | typeid               | id        |
      | Daniela | Pico     | 3258975863 | Cédula de ciudadanía | 1234567889|
    Then I validate the success message

  @UnSuccessAddDoctor
  Scenario: Add new doctor
    Given I want to access the doctor form
    When A new doctor is hired
      | name    | lastname | telephone  | typeid               | id         |
      | Daniela | Pico     | 3258975863 | Cédula de ciudadanía | 1587987144 |
    Then I validate the unsuccess message
    
  @SuccessAddPatient
  Scenario: Register a new patient 
    Given I want to access the patient form
    When A new patient is added
    |name   |lastname|telephone |typeid    |id    |
    |Roberto|Carlos  |3507391262|Pasaportes|123456|
    Then Successful registration patient

  @SuccessCreateAppointment
  Scenario: Create Appointment
    Given I want to access the add appointment form
    When Create an appointment
    |date      |idpatient|iddoctor  |observations      |
    |27/03/2019|123456   |1020798882|Esto es una prueba|
    Then Successful registration appointment
