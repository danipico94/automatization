package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.AddNewDoctorValidationSteps;
import com.choucair.formacion.steps.AddNewPatientValidationSteps;
import com.choucair.formacion.steps.CreateAppointmentValidationSteps;
import com.choucair.formacion.steps.HospitalMenuValidationSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class AddNewDoctorValidationDefinition {
	
	@Steps
	AddNewDoctorValidationSteps addNewDoctorValidationSteps;
	
	@Steps
	AddNewPatientValidationSteps addNewPatientValidationSteps;
	
	@Steps
	CreateAppointmentValidationSteps createAppointmentValidationSteps;

	@Steps
	HospitalMenuValidationSteps hospitalMenuValidationSteps;
	
	
	@Given("^I want to access the doctor form$")
	public void i_want_to_register_a_new_doctor() throws Exception {
    hospitalMenuValidationSteps.doctor_form_validation();
	}
	
	@Given("^I want to access the patient form$")
	public void i_want_to_register_a_new_patient() throws Exception {
    hospitalMenuValidationSteps.patient_form_validation();
	}
	
	@Given("^I want to access the add appointment form$")
	public void i_want_to_register_a_new_appointment() throws Exception {
	hospitalMenuValidationSteps.appointment_form_validation();
	}
		
	@When("^A new doctor is hired$")
	public void a_new_doctor_is_hired(DataTable dtDataForm) throws InterruptedException{
        List<List<String>> data =dtDataForm.raw();
		Thread.sleep(5000);

		for (int i=1; i<data.size(); i++) {
			//Thread.sleep(5000);
			addNewDoctorValidationSteps.fill_form_data_table(data, i);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
			
			}
		}
	}
	@When("^A new patient is added$")
	public void add_new_Register_of_patient_in_Administración_de_Hospitales(DataTable dtDatosForm) throws InterruptedException {
		List<List<String>> data =dtDatosForm.raw();
		Thread.sleep(5000);
		for (int i=1; i<data.size(); i++) {
			
			addNewPatientValidationSteps.fill_patient_form_data_table(data, i);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
			}
		}
	}
	
	//	Scenario: Add appointment
	@When("^Create an appointment$")
	public void create_appointment_in_Admin(DataTable dtDatosForm) throws InterruptedException  {
		List<List<String>> data =dtDatosForm.raw();
		Thread.sleep(5000);
		for (int i=1; i<data.size(); i++) {
			
			createAppointmentValidationSteps.fill_appointment_form_data_table(data, i);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
			}
		}
	}
	
	//Scenario: Validation Register a patient
	@Then("^Successful registration patient$")
	public void successful_Registration() {
	    
	}
	
//	@Then("^I validate the unsuccess message$")
//	public void i_validate_the_unsuccess_message() throws Exception{
//		addNewDoctorValidationSteps.verify_success_fill_with_errors();
//	}

	@Then("^I validate the success message$")
	public void i_validate_the_success_message() {
		addNewDoctorValidationSteps.verify_success_fill_without_errors();	
	}
	
	

}
