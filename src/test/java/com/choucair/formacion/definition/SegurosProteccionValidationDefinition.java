package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.SegurosProteccionValidationSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class SegurosProteccionValidationDefinition {
	
	@Steps
	SegurosProteccionValidationSteps segurosProteccionValidationSteps;
	
	@Given("^Quiero acceder a la pagina de condiciones de uso$")
	public void quiero_acceder_a_la_pagina_de_condiciones_de_uso() throws Exception {
	 segurosProteccionValidationSteps.condiciones_uso();

	}

	@When("^Diligencio los campos nombres y apellidos$")
	public void diligencio_los_campos_nombres_y_apellidos(DataTable datos) throws InterruptedException{
        List<List<String>> dato =datos.raw();
		Thread.sleep(5000);

		for (int i=1; i<dato.size(); i++) {
			//Thread.sleep(5000);
			segurosProteccionValidationSteps.llenar_nombres(dato, i);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
			
			}
		}
	}

	@When("^Accedo al link mas ventajas$")
	public void accedo_al_link_mas_ventajas() {
	segurosProteccionValidationSteps.mas_ventajas();
	}

	@When("^Diligencio tipo y numero de documento$")
	public void diligencio_tipo_y_numero_de_documento(DataTable datos) throws InterruptedException{
        List<List<String>> dato =datos.raw();
		Thread.sleep(5000);

		for (int i=1; i<dato.size(); i++) {
			segurosProteccionValidationSteps.documento(dato, i);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
			
			}
		}
	}

	@When("^Diligencio la fecha$")
	public void diligencio_la_fecha(DataTable datos) throws InterruptedException{
        List<List<String>> dato =datos.raw();
		Thread.sleep(5000);

		for (int i=1; i<dato.size(); i++) {
			segurosProteccionValidationSteps.fechaExp(dato, i);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
			
			}
		}
	}

	@Then("^Valido la salida$")
	public void valido_la_salida() {
		segurosProteccionValidationSteps.valido_mensaje_error();
	}


}
