package com.choucair.formacion.pageobjects;

import org.openqa.selenium.Keys;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class CreateAppointmentValidationPage extends PageObject{
	//Field Date
	@FindBy(xpath="//*[@id='datepicker']")
	public WebElementFacade fromDateBox;

	//Field Identification patient
 	@FindBy(xpath="//*[@id='page-wrapper']/div/div[3]/div/div[2]/input") 
 	public WebElementFacade txtidpatient;
 	
 	//Field Identification doctor
 	@FindBy(xpath="//*[@id='page-wrapper']/div/div[3]/div/div[3]/input")
 	public WebElementFacade txtiddoctor;
 
 	//Field observations
 	@FindBy(xpath="//*[@id='page-wrapper']/div/div[3]/div/div[4]/textarea")
 	public WebElementFacade txtobservations;
	
 	//Field Button Save
 	@FindBy(xpath="//*[@id='page-wrapper']/div/div[3]/div/a")
 	public WebElementFacade btnSave;
 	
 	
	 public void Date (String testData) {
		 fromDateBox.sendKeys(testData);
		 fromDateBox.sendKeys(Keys.ENTER);
	 }
		
	 public void IdPatient (String testData) {
		 txtidpatient.click();
		 txtidpatient.clear();
		 txtidpatient.sendKeys(testData);
	 }
	 
	 public void IdDoctor (String testData) {
		 txtiddoctor.click();
		 txtiddoctor.clear();
		 txtiddoctor.sendKeys(testData);
	 }
	 
	 public void Observation (String testData) {
		 txtobservations.click();
		 txtobservations.clear();
		 txtobservations.sendKeys(testData);
	 }
	 
	 public void Save(){
		 btnSave.click();
	 }
	 	 

}
