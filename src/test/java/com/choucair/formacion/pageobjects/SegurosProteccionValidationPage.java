package com.choucair.formacion.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

@DefaultUrl("https://www.proteccion.com/wps/portal/proteccion/web/home/general/solicita-clave")
public class SegurosProteccionValidationPage extends PageObject {
	

	
	 //Seccion condiciones de uso
	@FindBy(xpath="//*[@id=\'section-main\']/div/div[1]/div[1]/div/section/div/div[2]/div[4]/div/div/h1/a/p")
	public WebElement menuCondicionesUso;
	
	 //Seccion Validacion identidad
	@FindBy(xpath="//*[@id='izquierdoAct\']/div[1]/h2")
	public WebElement menuValidacionIdentidad;
	
	 //Seccion Más ventajas
	@FindBy(xpath="//*[@id=\'tab2\']/span")
	public WebElement linkMasVentajas;

	
	 //Campo Nombres
//	@FindBy(xpath="//*[@id='nombres']") 
//	public WebElementFacade txtNombres;
	 //Campo Primer Apellido
	@FindBy(xpath="//*[@id='primerApellido']")
	public WebElementFacade txtPrimerApellido;
	
	 //Campo Primer Apellido
	@FindBy(xpath="//*[@id='segundoApellido']")
	public WebElementFacade txtSegundoApellido;
	
	 //Campo Tipo identificacion
	@FindBy(xpath="//*[@id=\'tipoIdentificacion\']")  
	public WebElementFacade cmbTipoIdentificacion;

	
	 //Campo identificacion
	@FindBy(xpath="//*[@id=\'identificacion\']")  
	public WebElementFacade txtidentificacion;
	
	 //Campo identificacion
	@FindBy(xpath="	//*[@id=\'fechaExpedicion\']")  
	public WebElementFacade txtFechaExp;
	
     //Campo Mensaje
	@FindBy(xpath="	//*[@id=\'popup_message\']")  
	public WebElementFacade txtmensaje;

	
    //Botón Validar entidad 
	@FindBy(xpath="//*[@id=\"izquierdoAct\"]/div[5]/a")  
	public WebElementFacade btnValidaIdentidad;
	
	public void menuCondicionesUsoValidacion() {
		menuCondicionesUso.click();
		Serenity.takeScreenshot();
		 Robot robot;
			try {
				robot = new Robot();
				robot.keyPress(KeyEvent.VK_PAGE_DOWN);
			} catch (AWTException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}
	
	public void Nombres(String tablaDatos){
		WebDriver driver = getDriver();

		driver.switchTo().frame(driver.findElement(By.id("contenido"))).switchTo().frame(driver.findElement(By.id("contenido2")));
		WebElement iframeElement = driver.findElement(By.id("nombres"));
		iframeElement.click();
		iframeElement.sendKeys(tablaDatos);
		
//		txtNombres.clear();
//		txtNombres.sendKeys(tablaDatos);
	}
	
	public void PrimerApellido(String tablaDatos) {
		txtPrimerApellido.click();
		txtPrimerApellido.sendKeys(tablaDatos);
	}
	
	public void SegundoApellido(String tablaDatos) {
		txtSegundoApellido.click();
		txtSegundoApellido.sendKeys(tablaDatos);
	}
	
	public void MasVentajas() {
	linkMasVentajas.click();
	Serenity.takeScreenshot();
	}
	
	
	public void TipoDocumento(String tablaDatos) {
		cmbTipoIdentificacion.click();
		cmbTipoIdentificacion.selectByVisibleText(tablaDatos);
	}
	public void Documento(String tablaDatos) {
		 Robot robot;
			try {
				robot = new Robot();
				robot.keyPress(KeyEvent.VK_PAGE_DOWN);
			} catch (AWTException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		txtidentificacion.click();
		txtidentificacion.sendKeys(tablaDatos);
	}
	
	public void Fecha(String tablaDatos) {
		Robot robot;
		try {
			robot = new Robot();
			robot.keyPress(KeyEvent.VK_PAGE_DOWN);
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		txtFechaExp.click();
		txtFechaExp.sendKeys(tablaDatos);
	}
	
	
	public void Valida_identidad() {
		btnValidaIdentidad.click();
	}
	
	
	public void Valida_Mensaje() {
		assertThat(txtmensaje.isCurrentlyVisible(), is(true));
		Serenity.takeScreenshot();
   
	}

}
