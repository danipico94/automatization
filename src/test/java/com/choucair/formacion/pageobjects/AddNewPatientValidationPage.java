package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class AddNewPatientValidationPage extends PageObject{
    //Field Name 
	@FindBy(xpath="//*[@id='page-wrapper']/div/div[3]/div/div[1]/input")
	public WebElementFacade txtName;

	//Field Last_Name 
 	@FindBy(xpath="//*[@id='page-wrapper']/div/div[3]/div/div[2]/input")
 	public WebElementFacade txtLast_Name;
 	
 	//Field Telephone
 	@FindBy(xpath="//*[@id='page-wrapper']/div/div[3]/div/div[3]/input")
 	public WebElementFacade txtTelephon;
 
 	//Field Identication_Type
 	@FindBy(xpath="//*[@id='page-wrapper']/div/div[3]/div/div[4]/select")
 	public WebElementFacade cmbIdentification_Type;
 
 	//Field  Number Identification
 	@FindBy(xpath="//*[@id='page-wrapper']/div/div[3]/div/div[5]/input")
 	public WebElementFacade txtIdentification;

 	//Field Check prepaid medicine
 	@FindBy(xpath="//*[@id='page-wrapper']/div/div[3]/div/div[6]/label/input")
 	public WebElementFacade chkLbhealth;
 	
 	//Field Button Save
 	@FindBy(xpath="//*[@id=\'page-wrapper']/div/div[3]/div/a")
 	public WebElementFacade btnSave;
 	
 	//Campo date Earlier
 	@FindBy(xpath="(//DIV[@class='formErrorContent'])[1]")
 	public WebElementFacade globoInformativo ;
 	
	 public void Name (String testData) {
		 txtName.click();
		 txtName.clear();
		 txtName.sendKeys(testData);
	 }
 
	 public void Last_Name (String testData) {
		 txtLast_Name.click();
		 txtLast_Name.clear();
		 txtLast_Name.sendKeys(testData);
	 }
 
	 public void Telephone (String testData) {
		 txtTelephon.click();
		 txtTelephon.clear();
		 txtTelephon.sendKeys(testData);
	 }
 
	 public void Identication_Type (String testData) {
		cmbIdentification_Type.click();
		cmbIdentification_Type.selectByVisibleText(testData);
	    
	 }
	 
	 public void Identification (String testData) {
		 txtIdentification.click();
		 txtIdentification.clear();
		 txtIdentification.sendKeys(testData);
	 }
	 
	 public void Check(){
		 chkLbhealth.click();
	 }
	 public void Save(){
		 btnSave.click();
	 }		 	

}

