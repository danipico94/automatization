package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class AddNewDoctorValidationPage extends PageObject{
	 //Field Name 
	@FindBy(xpath="//*[@id='name']")
	public WebElementFacade txtName;

 //Field Last_Name 
 	@FindBy(xpath="//*[@id='last_name']")
 	public WebElementFacade txtLast_Name;
 	
 //Field Telephone
 	@FindBy(xpath="//*[@id='telephone']")
 	public WebElementFacade txtTelephone;
 
 //Field Identication_Type
 	@FindBy(xpath="//*[@id='identification_type']")
 	public WebElementFacade cmbIdentification_Type;
 
 //Field  Number Identification
 	@FindBy(xpath="//*[@id='identification']")
 	public WebElementFacade txtIdentification;
 	
 //Field  Save Button
 	@FindBy(xpath="//*[@id='page-wrapper']/div/div[3]/div/a")
 	public WebElementFacade btnSave;
 	
  //Field confirmation
	@FindBy(xpath="//*[@id=\"page-wrapper\"]/div/div[2]/div[1]/h3")        
	public WebElementFacade messageInformative;

 	
	 public void Name (String dataTest) {
		 txtName.click();
		 txtName.clear();
		 txtName.sendKeys(dataTest);
	 }
	 
	 public void Last_Name (String dataTest) {
		 txtLast_Name.click();
		 txtLast_Name.clear();
		 txtLast_Name.sendKeys(dataTest);
	 }
 
	 public void Telephone (String dataTest) {
		 txtTelephone.click();
		 txtTelephone.clear();
		 txtTelephone.sendKeys(dataTest);
	 }
 
	 public void Identication_Type (String dataTest) {
		 cmbIdentification_Type.click();
		 cmbIdentification_Type.selectByVisibleText(dataTest);
		 
	 }
	 
	 public void Identification (String dataTest) {
		 txtIdentification.click();
		 txtIdentification.clear();
		 txtIdentification.sendKeys(dataTest);
	 }
	 
	 public void Save() {
	    	btnSave.click();
	 }
	 
	public void form_without_errors() {
		assertThat(messageInformative.isCurrentlyVisible(), is(false));
   
	}
 
//    public void form_with_errors() {
//    	assertThat(messageInformative.isCurrentlyVisible(), is(true));
//    }


}
