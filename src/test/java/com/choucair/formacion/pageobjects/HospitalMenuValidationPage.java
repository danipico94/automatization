
package com.choucair.formacion.pageobjects;

import org.openqa.selenium.WebElement;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

@DefaultUrl("http://automatizacion.herokuapp.com/pperez/")
public class HospitalMenuValidationPage extends PageObject{
	
	 //Form Add New Doctor
	@FindBy(xpath="	//*[@id=\'page-wrapper\']/div/div[2]/div/div/div/div/div[1]/div/a[1]")
	public WebElement menuDoctor;
	
	 //Form Add New Patient
	@FindBy(xpath="	//*[@id=\'page-wrapper\']/div/div[2]/div/div/div/div/div[1]/div/a[2]")
	public WebElement menuPatient;		
	
	 //Form Add Appointment Scheduling
	@FindBy(xpath="	//*[@id=\'page-wrapper\']/div/div[2]/div/div/div/div/div[1]/div/a[6]")
	public WebElement menuAppointmentScheduling;	


	
	public void menuDoctorValidation() {
		menuDoctor.click();
	}
	
	public void menuPatientValidation() {
		menuPatient.click();
	}
	
	public void addAppointmentSchedulingValidation() {
		menuAppointmentScheduling.click();
	}

}
