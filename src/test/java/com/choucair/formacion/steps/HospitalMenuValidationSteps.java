package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.HospitalMenuValidationPage;

import net.thucydides.core.annotations.Step;

public class HospitalMenuValidationSteps {
	HospitalMenuValidationPage hospitalMenuValidationPage;

	@Step
	public void doctor_form_validation() {
		hospitalMenuValidationPage.open();
		hospitalMenuValidationPage.menuDoctorValidation();
		
	}
	
	@Step
	public void patient_form_validation() {
		hospitalMenuValidationPage.open();
		hospitalMenuValidationPage.menuPatientValidation();
		
	}
	
	@Step
	public void appointment_form_validation() {
		hospitalMenuValidationPage.open();
		hospitalMenuValidationPage.addAppointmentSchedulingValidation();
		
	}
}
