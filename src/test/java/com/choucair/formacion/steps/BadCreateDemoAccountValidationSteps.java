package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.BadCreateDemoAccountValidationPage;

import net.thucydides.core.annotations.Step;

public class BadCreateDemoAccountValidationSteps {
	
	BadCreateDemoAccountValidationPage badCreateDemoAccountValidationPage;
	
	@Step	
	public void bad_fill_form_demo_account(List<List<String>>data, int id) throws InterruptedException {

		badCreateDemoAccountValidationPage.FirstName(data.get(id).get(0).trim());
		badCreateDemoAccountValidationPage.LastName(data.get(id).get(1).trim());
		badCreateDemoAccountValidationPage.Country(data.get(id).get(2).trim());
		badCreateDemoAccountValidationPage.City(data.get(id).get(3).trim());
		badCreateDemoAccountValidationPage.Phone(data.get(id).get(4).trim());	
		badCreateDemoAccountValidationPage.Email(data.get(id).get(5).trim());
		badCreateDemoAccountValidationPage.Language(data.get(id).get(6).trim());
		badCreateDemoAccountValidationPage.Close();
		badCreateDemoAccountValidationPage.Password1(data.get(id).get(7).trim());
		badCreateDemoAccountValidationPage.Password2("");
		badCreateDemoAccountValidationPage.Save();
		badCreateDemoAccountValidationPage.Password2(data.get(id).get(8).trim());
		badCreateDemoAccountValidationPage.Save();
	
	}
	
	@Step
	public void validate_bad_message_confirmation(){
		badCreateDemoAccountValidationPage.Form_without_errors();
	}

}
