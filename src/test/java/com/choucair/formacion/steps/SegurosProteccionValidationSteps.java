package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.SegurosProteccionValidationPage;

import net.thucydides.core.annotations.Step;

public class SegurosProteccionValidationSteps {
	SegurosProteccionValidationPage segurosProteccionValidationPage;
	
	@Step
	public void condiciones_uso() {
		segurosProteccionValidationPage.open();
		segurosProteccionValidationPage.menuCondicionesUsoValidacion();
		
	}
	
	@Step
	public void llenar_nombres(List<List<String>>dato, int id) throws InterruptedException {
		segurosProteccionValidationPage.open();
		segurosProteccionValidationPage.Nombres(dato.get(id).get(0).trim());
		segurosProteccionValidationPage.PrimerApellido(dato.get(id).get(1).trim());
		segurosProteccionValidationPage.SegundoApellido(dato.get(id).get(2).trim());
	}
	
	@Step
	public void mas_ventajas(){
		segurosProteccionValidationPage.MasVentajas();
	}
	
	@Step
	public void documento(List<List<String>>dato, int id) throws InterruptedException {
		segurosProteccionValidationPage.TipoDocumento(dato.get(id).get(0).trim());
		segurosProteccionValidationPage.Documento(dato.get(id).get(1).trim());
	}
	
	@Step
	public void fechaExp(List<List<String>>dato, int id) throws InterruptedException {
		segurosProteccionValidationPage.Fecha(dato.get(id).get(0).trim());
	}
	
	@Step
	public void valido_mensaje_error(){
		segurosProteccionValidationPage.Valida_identidad();
		segurosProteccionValidationPage.Valida_Mensaje();
	}

}
