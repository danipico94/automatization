package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.CreateAppointmentValidationPage;

import net.thucydides.core.annotations.Step;


public class CreateAppointmentValidationSteps {
	
 CreateAppointmentValidationPage createAppointmentValidationPage;
	
	@Step 
	public void fill_appointment_form_data_table(List<List<String>>data, int id) {
	createAppointmentValidationPage.Date(data.get(id).get(0).trim());
	createAppointmentValidationPage.IdPatient(data.get(id).get(1).trim());
	createAppointmentValidationPage.IdDoctor(data.get(id).get(2).trim());
	createAppointmentValidationPage.Observation(data.get(id).get(3).trim());
	createAppointmentValidationPage.Save();
	 
	}


}
