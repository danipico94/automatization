package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.AddNewPatientValidationPage;

import net.thucydides.core.annotations.Step;

public class AddNewPatientValidationSteps {
	AddNewPatientValidationPage addNewPatientValidationPage;
	
	@Step	
	public void fill_patient_form_data_table(List<List<String>>data, int id) {
		addNewPatientValidationPage.Name(data.get(id).get(0).trim());
		addNewPatientValidationPage.Last_Name(data.get(id).get(1).trim());
		addNewPatientValidationPage.Telephone(data.get(id).get(2).trim());
		addNewPatientValidationPage.Identication_Type(data.get(id).get(3).trim());
		addNewPatientValidationPage.Identification(data.get(id).get(4).trim());	
		addNewPatientValidationPage.Check();
		addNewPatientValidationPage.Save();	
	}
	
}
