package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.AutogeneratePasswordValidationPage;

import net.thucydides.core.annotations.Step;

public class AutogeneratePasswordValidationSteps {

	AutogeneratePasswordValidationPage autogeneratePasswordValidationPage;
	
	@Step	
	public void fill_form_autogenerate_password_account(List<List<String>>data, int id) throws InterruptedException {

		autogeneratePasswordValidationPage.FirstName(data.get(id).get(0).trim());
		autogeneratePasswordValidationPage.LastName(data.get(id).get(1).trim());
		autogeneratePasswordValidationPage.Country(data.get(id).get(2).trim());
		autogeneratePasswordValidationPage.City(data.get(id).get(3).trim());
		autogeneratePasswordValidationPage.Phone(data.get(id).get(4).trim());	
		autogeneratePasswordValidationPage.Email(data.get(id).get(5).trim());
		autogeneratePasswordValidationPage.Language(data.get(id).get(6).trim());
		autogeneratePasswordValidationPage.Close();
		autogeneratePasswordValidationPage.Autopassword();
		autogeneratePasswordValidationPage.Save();
	
	}
}
