package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.AddNewDoctorValidationPage;

import net.thucydides.core.annotations.Step;

public class AddNewDoctorValidationSteps {


	AddNewDoctorValidationPage addNewDoctorValidationPage;

	@Step	
	public void fill_form_data_table(List<List<String>>data, int id) {
		addNewDoctorValidationPage.Name(data.get(id).get(0).trim());
		addNewDoctorValidationPage.Last_Name(data.get(id).get(1).trim());
		addNewDoctorValidationPage.Telephone(data.get(id).get(2).trim());
		addNewDoctorValidationPage.Identication_Type(data.get(id).get(3).trim());
		addNewDoctorValidationPage.Identification(data.get(id).get(4).trim());	
		addNewDoctorValidationPage.Save();
	
	}
	@Step
	public void verify_success_fill_without_errors() {
		addNewDoctorValidationPage.form_without_errors();
	}
	
//	@Step
//	public void verify_success_fill_with_errors() {
//		addNewDoctorValidationPage.form_with_errors();
//	}
	

}
