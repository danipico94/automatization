package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.FieldDemoAccountValidationPage;

import net.thucydides.core.annotations.Step;

public class FieldDemoAccountValidationSteps {
	
	FieldDemoAccountValidationPage  fieldDemoAccountValidationPage;
	
	
	@Step	
	public void fill_form__field_demo_account(List<List<String>>data, int id) throws InterruptedException {

		fieldDemoAccountValidationPage.FirstNameBad(data.get(id).get(0).trim());
		fieldDemoAccountValidationPage.LastName(data.get(id).get(1).trim());
		fieldDemoAccountValidationPage.FirstNameGood(data.get(id).get(2).trim());
		fieldDemoAccountValidationPage.LastName(data.get(id).get(1).trim());
		fieldDemoAccountValidationPage.Country(data.get(id).get(3).trim());
		fieldDemoAccountValidationPage.City(data.get(id).get(4).trim());
		fieldDemoAccountValidationPage.Phone(data.get(id).get(5).trim());	
		fieldDemoAccountValidationPage.Email(data.get(id).get(6).trim());
		fieldDemoAccountValidationPage.Language(data.get(id).get(7).trim());
		fieldDemoAccountValidationPage.Close();
		fieldDemoAccountValidationPage.Password1(data.get(id).get(8).trim());
		fieldDemoAccountValidationPage.Password2(data.get(id).get(9).trim());
		fieldDemoAccountValidationPage.Save();
	
	}
	
	@Step
	public void validate_message_confirmation(){
		fieldDemoAccountValidationPage.Form_without_errors();
	}
	
	

}
