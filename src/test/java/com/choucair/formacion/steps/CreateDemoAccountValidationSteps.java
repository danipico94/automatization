package com.choucair.formacion.steps;

import java.util.List;


import com.choucair.formacion.pageobjects.CreateDemoAccountValidationPage;

import net.thucydides.core.annotations.Step;

public class CreateDemoAccountValidationSteps {
	
	CreateDemoAccountValidationPage createDemoAccountValidationPage;
	
	
	@Step
	public void open_demo_account() {
	createDemoAccountValidationPage.open();	
	createDemoAccountValidationPage.Continue();

	}
	
	@Step	
	public void fill_form_demo_account(List<List<String>>data, int id) throws InterruptedException {

		createDemoAccountValidationPage.FirstName(data.get(id).get(0).trim());
		createDemoAccountValidationPage.LastName(data.get(id).get(1).trim());
		createDemoAccountValidationPage.Country(data.get(id).get(2).trim());
		createDemoAccountValidationPage.City(data.get(id).get(3).trim());
		createDemoAccountValidationPage.Phone(data.get(id).get(4).trim());	
		createDemoAccountValidationPage.Email(data.get(id).get(5).trim());
		createDemoAccountValidationPage.Language(data.get(id).get(6).trim());
		createDemoAccountValidationPage.Close();
		createDemoAccountValidationPage.Password1(data.get(id).get(7).trim());
		createDemoAccountValidationPage.Password2(data.get(id).get(8).trim());
		createDemoAccountValidationPage.Save();
	
	}
	
	@Step
	public void validate_message_confirmation(){
		createDemoAccountValidationPage.Form_without_errors();
	}

}
